INTRODUCTION
------------

Light module for locking nodes. Nodes are protected against paralell editation. 
Locks are bound to users.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/petr.hajek/2665184

 * To submit bug reports and feature suggestions, or to track changes:
   hhttps://www.drupal.org/project/issues/2665184


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information

CONFIGURATION
-------------

 * Configure which node types should be locked by this module in 
   Administration » Configuration » Content authoring » Lock system

FAQ
---

Q: How to unlock a node when leaving the editation page?

A: Click the Cancel or Save button.

Q: Node administration is being not locked. Why?

A: You need to configure lockable node types in Lock system administration. 

Q: Which users can take the editation lock of another users?

A: All users with permission "administer checked out documents".


MAINTAINERS
-----------

Current maintainers:
 * Petr Hájek (petr.hajek) - https://www.drupal.org/u/petr.hajek
 * Petr Stuchlík (stuchl4n3k) - https://www.drupal.org/u/stuchl4n3k


This project has been sponsored by:
 * Aira GROUP s.r.o.
  Specialist team AIRA operates in Czech Republic’s market since 1998. 
  Aira GROUP, s.r.o. is owned by Czech individuals and the company is 
  not owned by any other higher body.

  At the beginning we specialised in support and management of information 
  systems and in IT consulting. Gradually we broadened our services to cover 
  other areas of information technologies. In 2001, we put together a dedicated
  team for various IT products training with focus on office applications, 
  CAD tools and computer graphics tools. Now we also provide sophisticated 
  services in online marketing.
  
  Visit http://www.aira.cz for more information.