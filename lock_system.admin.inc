<?php

/**
 * @file
 * File containing aministration forms.
 */

/**
 * Form for settings of Lock system module.
 *
 * @return array
 *   Settings form.
 */
function lock_system_admin_settings() {
  $form = array();
  $all_node_types = array();

  foreach (node_type_get_types() as $type_name => $node_type) {
    $all_node_types[$type_name] = $node_type->name;
  }
  $form['lock_system_allowed_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select node types on which will be used Lock system'),
    '#options' => $all_node_types,
    '#default_value' => variable_get('lock_system_allowed_node_types', array()),
  );
  return system_settings_form($form);
}
